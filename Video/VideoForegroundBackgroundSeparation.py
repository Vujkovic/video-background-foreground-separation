#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 30 23:35:19 2022

@author: milan
"""
import moviepy
import moviepy.editor as mpe
import time
# from IPython.display import display
from glob import glob
import sys, os
import numpy as np
import scipy
import matplotlib.pyplot as plt
from PIL import Image


def create_data_matrix_from_video(clip, dims, k=5, scale=50):
    return np.vstack([np.array(Image.fromarray(rgb2gray(clip.get_frame(i/float(k))).astype(np.uint8)).resize(size=(dims[1],dims[0]))).flatten() 
                      for i in range(k * int(clip.duration))]).T

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])

def plt_images(M, A, E, index_array, dims, filename=None):
    f = plt.figure(figsize=(15, 10))
    r = len(index_array)
    pics = r * 3
    for k, i in enumerate(index_array):
        for j, mat in enumerate([M, A, E]):
            sp = f.add_subplot(r, 3, 3*k + j + 1)
            sp.axis('Off')
            pixels = mat[:,i]
            if isinstance(pixels, scipy.sparse.csr_matrix):
                pixels = pixels.todense()
            plt.imshow(np.reshape(pixels, dims), cmap='gray')
    return f

def plots(ims, dims, figsize=(15,20), rows=1, interp=False, titles=None):
    if type(ims[0]) is np.ndarray:
        ims = np.array(ims)
    f = plt.figure(figsize=figsize)
    for i in range(len(ims)):
        sp = f.add_subplot(rows, len(ims)//rows, i+1)
        sp.axis('Off')
        plt.imshow(np.reshape(ims[i], dims), cmap="gray")

video = mpe.VideoFileClip("rogue2.mp4")
video = video.subclip(4,7)#11,17 #1,3 #4,7
scale = 20 # Adjust scale to change resolution of image
frames = 60
dims = (int(video.size[1] * (scale/100)), int(video.size[0] * (scale/100)))
M = create_data_matrix_from_video(video, dims, frames, scale)
print(dims, M.shape)

#M = M[:,885:915]

t= np.linspace(0,2*M.shape[1]/frames*np.pi,M.shape[1])
dt = t[1] - t[0]

plt.imshow(np.reshape(M[:,60], dims), cmap='gray');

plt.figure(figsize=(12, 12))
plt.imshow(M, cmap='gray')

#DMD
X1 = M[:,0:-1]
X2 = M[:,1:]

r = 30
U,S,V = np.linalg.svd(X1, full_matrices = False)
r = min(r, U.shape[1])
S = np.diag(S)
V = np.asmatrix(V).H

U_r = U[:,0:r]
S_r = S[0:r,0:r]
V_r = V[:,0:r]

Atilde=np.asmatrix(U_r).H @ X2 @ V_r @ np.linalg.pinv(S_r)
D, W_r = np.linalg.eig(Atilde)

Phi = X2 @ V_r @ np.linalg.inv(S_r) @ W_r

lamb = D
omega = np.log(lamb)/dt

plt.plot(np.real(lamb),np.imag(lamb), '.')
plt.plot(np.real(omega),np.imag(omega), '.')

#stestbg = np.where(np.abs(lamb)<1+1e-2)[0]
bg = np.where(np.abs(omega)<1e-2)[0]

fg = np.setdiff1d(np.array([i for i in range(r)]),bg)
omega_fg=omega[fg]
Phi_fg = Phi[:, fg];
omega_bg=omega[bg]
Phi_bg = Phi[:, bg];

#lamb_fg = lamb[fg]
#lamb_bg = lamb[bg]

x1 = X1[:,0]
b = np.linalg.lstsq(Phi_bg,x1, rcond=None)[0]
X_bg = np.zeros((omega_bg.size,len(t)))
for tt in range(len(t)):
    X_bg[:,tt] = (np.multiply(b, np.exp(omega_bg*t[tt])))#lamb_bg**tt

X_bg = Phi_bg @ X_bg

b = np.linalg.lstsq(Phi_fg,x1, rcond=None)[0]
X_fg = np.zeros((omega_fg.size,len(t)))
for tt in range(len(t)):
    X_fg[:,tt] = np.multiply(b,(np.exp(omega_fg*t[tt])))

X_fg = Phi_fg @ X_fg

X_sparse = M - abs(X_bg)
R = np.array(X_sparse)
R[R>0] = 0
X_bg = R + abs(X_bg)
X_sparse = X_sparse - R

plt.imshow(np.reshape(X_sparse[:,60], dims), cmap='gray');

#slideshow
# for i in range(M.shape[1]):
#     if i%5==0:
#         plt.imshow(np.reshape(X_sparse[:,i], dims), cmap='gray');
#         # plt.imshow(np.reshape(M[:,i], dims), cmap='gray');
#         # plt.imshow(np.reshape((np.imag(X_fg))[:,i], dims), cmap='gray');
#         plt.show()
#         time.sleep(0.1)