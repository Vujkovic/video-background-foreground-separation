#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 19:06:27 2022

@author: milan
"""
import numpy as np

xi = np.linspace(-10,10,400)
t = np.linspace(0,4*np.pi,200)
dt = t[1] - t[0]

[Xgrid, T] = np.meshgrid(xi,t);
f1 = np.multiply(1/np.cosh(Xgrid + 3),1*np.exp(1j*2.3*T))
f2 = np.multiply(np.multiply(1/np.cosh(Xgrid), np.tanh(Xgrid)),2*np.exp(1j*2.8*T))
f = f1+f2

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(Xgrid, T, np.real(f1), cmap=cm.magma)
ax.view_init(-125, 75)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(Xgrid, T, np.real(f2), cmap=cm.magma)
ax.view_init(-125, 75)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(Xgrid, T, np.real(f), cmap=cm.magma)
ax.view_init(-125, 75)
plt.show()

X = np.transpose(f)
np.matrix(X)
X1 = X[:,0:-1]
X2 = X[:,1:]
r = 2
dt=t[1]

#Phi,omega,lamb,b,Xdmd = DMD(X1,X2,r,dt)

U,S,V = np.linalg.svd(X1, full_matrices = False)#greska ovde?
r = min(r, U.shape[1])
S = np.diag(S)
V = np.asmatrix(V).H

U_r = U[:,0:r]
S_r = S[0:r,0:r]
V_r = V[:,0:r]

#Atilde=np.asmatrix(U_r).H @ X2 @ V_r @ np.linalg.inv(S_r)
Atilde=np.linalg.lstsq( np.asmatrix(U_r).H @ X2 @ V_r,  S_r,rcond=None)[0]
D, W_r = np.linalg.eig(Atilde)

Phi = X2 @ V_r @ np.linalg.inv(S_r) @ W_r

lamb = D
omega = np.log(lamb)/dt

x1 = X1[:,0]
b = np.linalg.lstsq(Phi,x1, rcond=None)[0]
mm1 = X1.shape[1];
time_dynamics = np.zeros((r,mm1+1))
t = np.array([i*dt for i in range(mm1+1)])
for i in range(mm1+1):
    time_dynamics[:,i] = (np.multiply(b,np.exp(omega*t[i])))

X_dmd = Phi @ time_dynamics;

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(Xgrid, T, np.real(np.asmatrix(X_dmd).H), cmap=cm.magma)
ax.view_init(-125, 75)
plt.show()
