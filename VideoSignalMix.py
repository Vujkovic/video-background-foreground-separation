#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 20 14:31:01 2022

@author: milan
"""
import numpy as np

n=200
m=80
xi = np.linspace(-15,15,n)
t = np.linspace(0,8*np.pi,m)
dt = t[1] - t[0]
[Xgrid, T] = np.meshgrid(xi,t);

f1 = np.multiply(0.5*np.cos(Xgrid),1+0*T)
f2 = np.multiply(np.multiply(1/np.cosh(Xgrid), np.tanh(Xgrid)),2*np.exp(1j*2.8*T))

X=np.asmatrix(f1+f2).H

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(Xgrid, T, np.real(np.asmatrix(X).H), cmap=cm.magma)
ax.view_init(-125, 75)
plt.show()

X1 = X[:,0:-1]
X2 = X[:,1:]
r = 5

U,S,V = np.linalg.svd(X1, full_matrices = False)#greska ovde?
r = min(r, U.shape[1])
S = np.diag(S)
V = np.asmatrix(V).H

U_r = U[:,0:r]
S_r = S[0:r,0:r]
V_r = V[:,0:r]

Atilde=np.linalg.lstsq(np.asmatrix(U_r).H @ X2 @ V_r,  S_r,rcond=None)[0]
D, W_r = np.linalg.eig(Atilde)

Phi = X2 @ V_r @ np.linalg.inv(S_r) @ W_r

lamb = D
omega = np.log(lamb)/dt

plt.plot(np.real(omega),np.imag(omega), '.')

bg = np.where(np.abs(omega)<1e-2)[0]
fg = np.setdiff1d(np.array([i for i in range(r)]),bg)
omega_fg=omega[fg]
Phi_fg = Phi[:, fg];
omega_bg=omega[bg]
Phi_bg = Phi[:, bg];

x1 = X1[:,0]

b = np.linalg.lstsq(Phi_bg,x1, rcond=None)[0]
X_bg = np.zeros((omega_bg.size,len(t)))
for tt in range(len(t)):
    X_bg[:,tt] = (np.multiply(b,np.exp(omega_bg*t[tt])))

X_bg = Phi_bg @ X_bg

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(Xgrid, T, np.real(np.asmatrix(X_bg).H), cmap=cm.magma)
ax.view_init(-125, 75)
plt.show()

b = np.linalg.lstsq(Phi_fg,x1, rcond=None)[0]#
X_fg = np.zeros((omega_fg.size,len(t)))
for tt in range(len(t)):    #49x1 
    X_fg[:,tt] = np.multiply(b,(np.exp(omega_fg*t[tt])).reshape(r-1,1)).reshape(-1)
    #X_fg[:,tt] = (np.multiply(b,np.asmatrix(np.exp(omega_fg*t[tt])).T)).reshape(-1)

X_fg = Phi_fg @ X_fg
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(Xgrid, T, np.real(np.asmatrix(X_fg).H), cmap=cm.magma)
#ax.plot_surface(Xgrid, T, np.real(np.asmatrix(X-X_bg).H), cmap=cm.magma)
ax.view_init(-125, 75)
plt.show()